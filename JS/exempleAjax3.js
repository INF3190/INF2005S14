/**
 * @author Johnny Tsheke
 */

$(document).ready(function(){
	$("#territoire").click(function(){ 
		$("#affiche").css({"display":"block"});
		$("#affiche").jstree({
			'core':{
				'data':{
					'url':function(node) { 
					 return node.id === "#" ?
					 '../JS/exempleAjax3.json'://url AJAX si racine
					 '../JS/exempleAjax3.json'; //url AJAX sinon	
					},
					'data': function (node) {
						return{'id': node.id};
					},
					'dataType':'json'
				}
			}
		});
	});
 
	$("#cacher").click(function(){
		$("#affiche").css({"display":"none"});
	});
	$("#basculer").click(function(){
		$("#affiche").toggle();
	});
});
