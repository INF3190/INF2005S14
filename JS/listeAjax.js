/**
 * @author Johnny Tsheke
 */
$.support.cors=true;
$(document).ready(function(){
	$("#liste").click(function(){
		$.ajax({
			type: "GET",
		url: "../XML/liste.xml",
		dataType: "xml",
		isLocal: true,
		error : function(error){alert("erreur telechargement du fichier XML");},
		success: function(xml){
		var etuds="<ol>";
		$(xml).find("etudiant").each(function(index){	
			etuds=etuds+ "<li>"+ $(this).text()+"</li>";
		});
		 etuds=etuds+"</ol>";
		 //$("#affiche").html(""+etuds+"");
		 //$("#affiche").append(etuds);
		 $("#affiche>ol").eq(0).remove();//supression affichage précédent
		 $("#affiche").prepend(etuds);
		}
		});
	});
	
}
);
