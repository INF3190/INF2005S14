/**
 * @author Johnny Tsheke --UQAM
 * Validation code postal canadien
 */
function validerCodePostal(){
	 //re="/[A-Z][0-9][A-Z]\s[0-9][A_Z][0-9]/i";//format de code postal H0H 1H2
	re=new RegExp("\\b[A-Z][0-9][A-Z]\\s[0-9][A-Z][0-9]\\b","i");
	$("#codePostal").keyup(function(){//quand le bouton se souleve
		texte=$(this).val().trim();
		if(re.test(texte))
		{ 
		msg="Code postal valide";
		$("#indicateur") //instruction en chainage commence ici
		.css({'color':'white','background-color':'green','width':'200px'})
		.html(msg);//instruction en chainage finie ici
		$(":submit").attr('disabled',false); //activation du bouton soumettre
		$(":submit").addClass('btn-success').removeClass('btn-default');
		}
		else{ 
			$(":submit").attr('disabled',true);//désactivation du bouton
			$(":submit").addClass('btn-default').removeClass('btn-success');
			msg="Code postal invalide";
			//msg=msg+" "+re.toString(); //pour voir l'expression réguliere
			$("#indicateur")
			.css({'color':'white','background-color':'red','width':'200px'})
			.html(msg);	
		}
	});
}

$(document).ready(function(){
	validerCodePostal();
});
