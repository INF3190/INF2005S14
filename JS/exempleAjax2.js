/**  @author Johnny Tsheke */

function contenuAjax()
{	
	var xmlhttp =new XMLHttpRequest();
	
  xmlhttp.onreadystatechange = function() {
  if((xmlhttp.readyState==4) && (xmlhttp.status==200))
    { 
    document.getElementById("ajaxContent").innerHTML=xmlhttp.responseText;
    }
    else if((xmlhttp.readyState==4) && (xmlhttp.status==404)){
    	document.getElementById("ajaxContent").innerHTML="Page non trouvée";
    }
 };
 
 var fichier="/infocours"; //url definit dans inf2005s13.js 
 xmlhttp.open("GET",fichier,true);//changez la méthode (GET,POST) puis tester
 xmlhttp.send("");
}
