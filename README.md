
Ce projet contient une application node.js permettant de tester les appels AJAX localement. A défaut de placer les fichiers sur un serveur web, vous pouvez installer nodejs et express.js puis démarrer l'application qui vous servir alors comme un serveur web local.

https://nodejs.org/

http://expressjs.com/

pour le démarrer:

node inf2005s14

L'application affichera le numéro de port  ()ex: 3000 sur lequel le serveur web est accessible. Vous pourriez alors accéder aux exemple en allant sur le site (dans le cas du port 3000) suivant

http://localhost:3000/